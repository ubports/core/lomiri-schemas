# Lomiri Schemas

## List of Schema Files Contained in this Project

  * com.lomiri.Shell

## Why a separate package for schema files?

The Lomiri schema files (Gsettings and AccountsSerivce schemata) are
required not only by Lomiri itself, but also by Ayatana Indicators
components. Bootstrapping Ayatana Indicators to a new distribution
becomes quite painful, if the full scope of the indicators (i.e. Lomiri
support) only becomes available once all Lomiri components have been
packaged for a distribution.

Also for CI builds of Ayatana Indicators and testing the Lomiri code, it
is essential to have the Lomiri related schema files easily accessible
(via the distribution or a small Git repository that gets build in build
env preparation phase).

To avoid the first and to provide support for the latter, the UBports
team agreed on splitting off the schema files (from src:pkg lomiri) into
an individual src:pkg / code project: lomiri-schemas.

(Actually, pretty similar to how the desktop and phablet teams at
Canonical Ltd. shipped schemas in the separate gsettings-ubuntu-schemas
package. (We finally understood now, why that was done).

## License and Copyright

For the license that applies to lomiri-schemas please see the COPYING file
shipped in the base folder of this project. At your option, also a newer
version of the license provided in COPYING may be applied.

Furthermore, the lomiri-schemas component is attributed by the following
copyright holders:

Copyright (C) 2021-2023 UBports Foundation  
Copyright (C) 2021 Robert Tari  
Copyright (C) 2010-2013 Canonical Ltd.

